'use strict';

var main_div = document.getElementById('main');
var pic_norm = document.getElementById('pic_norm');
var pic_poke = document.getElementById('pic_poke');
var music = make_audio_element({
  'audio/ogg': 'music.ogg',
  'audio/mpeg': 'music.mp3',
});
music.loop = true;
music.volume = 0.8;
var sound = make_audio_element({
  'audio/ogg': 'decision3.ogg',
  'audio/mpeg': 'decision3.mp3',
});
var react_sounds = [
  {
    'audio/ogg': 'react1.ogg',
    'audio/mpeg': 'react1.mp3',
  },
  {
    'audio/ogg': 'react2.ogg',
    'audio/mpeg': 'react2.mp3',
  },
];
var reaction = react_sounds.map(make_audio_element);
var pokes = 0;

function make_audio_element(source) {
  var audio_element = document.createElement('audio');
  Object.keys(source).forEach(function(key) {
    var source_element = document.createElement('source');
    source_element.type = key;
    source_element.src = source[key];
    audio_element.appendChild(source_element);
  });
  return audio_element;
}

function toggle_pic(press) {
  pic_norm.style.display = press ? 'none' : 'block';
  pic_poke.style.display = press ? 'block' : 'none';
}

function poke(em) {
  if (music.paused) music.play();
  sound.play();
  toggle_pic(true);
  if (++pokes % 6 === 0) {
    var ind = Math.floor(Math.random() * reaction.length);
    reaction[ind].play();
  }
}

function unpoke(em) {
  toggle_pic(false);
}

toggle_pic(false);

main_div.addEventListener('mousedown', function(ev) {
  if(ev.button !== 0) return;
  poke(ev);
});
main_div.addEventListener('mouseup', function(ev) {
  if(ev.button !== 0) return;
  unpoke(ev);
});
main_div.addEventListener('touchstart', function(ev) {
  poke(ev);
  ev.preventDefault();
});
main_div.addEventListener('touchmove', function(ev) {
  ev.preventDefault();
});
main_div.addEventListener('touchend', function(ev) {
  unpoke(ev);
});
main_div.addEventListener('touchcancel', function(ev) {
  unpoke(ev);
});